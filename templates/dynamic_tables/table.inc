{% comment %}
  name         table's #id, usually used for related pieces (js menu divs, etc)
  caption      text to use as table's caption
  action_url   URL for non-js action (menu?) or "falsey" to suppress
  action_text  text used in action link/button
  options_url  URL for table options menu or "falsey" to suppress
  view_name    name of view table appears in for table_options
  headers      data table headers
  groups       data table... data
  num_cols     how many are there?
  cls1,cls2    CSS classes for zebra striping table
  cls_break    CSS class for group break
{% endcomment %}
{% load tabletags %}
<table id="{{ name }}" cellspacing="0">
  <caption>{{ caption }}
    {% if action_url %}<div class="table_actions"><a href="{{ action_url }}?resume=here">{{ action_text }}</a></div>{% endif %}
    {% if options_url %}<div class="table_menu"><a href="{{ options_url }}?resume=here&view_name={{ view_name }}">options</a></div>{% endif %}
  </caption>
  {% if headers %}
    <thead><tr>{% for hd in headers %}{{ hd|th_cell }}{% endfor %}</tr></thead>
  {% endif %}
  {% for group,rows in groups %}
    {% if group.value %}<tr><td class="{{ cls_break }}" colspan="{{ num_cols }}">{{ group.value }}</td></tr>{% endif %}
    {% for item,cols in rows %}
      <tr class="{% modulo forloop.counter0 cls1 cls2 %}">{% for cd in cols %}{{ cd|td_cell }}{% endfor %}</tr>
    {% endfor %}
  {% endfor %}
</table>
