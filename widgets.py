from itertools import chain

from django.forms import Select
from django.forms.util import flatatt
from django.utils.encoding import force_unicode
#from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe


class OrderedSelectMultiple(Select):
    """
    Like SelectMultiple but with a different interface that allows the order of
    selected choices as well as the set of selections to be changed.
    
    This is not really intended for any existing data type, but for options.
    """

    allow_minus = False

    def render(self, name, value, attrs=None, choices=()):

        def append_choice(v, t, value=''):
            output.append(u'<li><input type="text" size="1" name="%s" value="%s" /> %s</li>'
                % (name + '_' + str(v), value, t))

        all_choices = [x for x in chain(self.choices, choices)]

        final_attrs = self.build_attrs(attrs, **{'class': 'choices'})

        ### FIX ME ### use conditional_escape appropriately

        output = [u'<ul%s>' % flatatt(final_attrs)]
        if value:
            cd = dict(all_choices)
            ### WORKAROUND ### 2.5.2 enumerate doesn't allow start argument
            for i,v in enumerate(value):
                i = i + 1
                if v in cd:
                    append_choice(v, cd.pop(v), i)
                if self.allow_minus and v[0] == '-' and v[1:] in cd:
                    append_choice(v[1:], cd.pop(v[1:]), -i)
            all_choices = [c for c in all_choices if c[0] in cd]
        for v,t in all_choices:
            append_choice(v, t)
        output.append(u'</ul>')
        if value:
            output.append(u'<input type="hidden" name="%s--ORIG-SEQUENCE" value="%s" />'
                % (name, ','.join(str(v) for v in value)))
        return mark_safe(u'\n'.join(output))

    def value_from_datadict(self, data, files, name):
        """
        a little tricky since it has to extract multiple items by the stem of the key

        NB: same-value inputs will sort unpredictably wrt each other
        """

        ### FIX ME ### just dropping non-integer input works, but gives no useful error

        kname = name + '_'
        kname_len = len(kname)
        items = []
        for k,v in data.items():
            if k.startswith(kname) and v.strip():
                try:
                    val = int(v.strip())
                except ValueError:
                    pass
                else:
                    items.append((k[kname_len:], val))

        items.sort(key=lambda x: abs(x[1]))
        return [(('-' if v < 0 else '') + k) for k,v in items]

    def _has_changed(self, initial, data):
        if initial is None:
            initial = []   
        if data is None:   
            data = []      
        if len(initial) != len(data):
            return True
        initial_norm = [force_unicode(value) for value in initial]
        data_norm = [force_unicode(value) for value in data]
        return data_norm != initial_norm
