from .widgets import OrderedSelectMultiple

from django.core.exceptions import ValidationError
from django.forms import MultipleChoiceField


class OrderedMultipleChoiceField(MultipleChoiceField):
    widget = OrderedSelectMultiple

    def __init__(self, *args, **kwargs):
        "adds allow_minus (default False) to permit negative order values & '-' prefix"

        allow_minus = kwargs.pop('allow_minus', False)
        super(OrderedMultipleChoiceField, self).__init__(*args, **kwargs)
        self.allow_minus = allow_minus
        self.widget.allow_minus = allow_minus

    def validate(self, value):
        "Validates that the input is a list or tuple"

        if self.required and not value:
            raise ValidationError(self.error_messages['required'])
        # Validate that each value in the value list is in self.choices.
        for val in value:
            if not self.valid_value(val) and (not self.allow_minus or not self.valid_value(val.lstrip('-'))):
                raise ValidationError(self.error_messages['invalid_choice'] % {'value': val})
