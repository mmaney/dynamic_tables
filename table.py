import datetime
from urllib import urlencode
try:
    from urlparse import parse_qsl
except ImportError:
    from cgi import parse_qsl         # it was here before 2.6
        

from .fields import OrderedMultipleChoiceField

from django.forms import CharField, Form, HiddenInput


### Generic list view mechanism rendering into HTML tables

NO_PERM = ''
COL_PERM = 'C'
KEY_PERM = 'K'
GRP_PERM = 'G'

COL_NAME = 'cols'
KEY_NAME = 'keys'
GRP_NAME = 'grps'

CKG_NAMES = (COL_NAME, KEY_NAME, GRP_NAME)

category_name = {
    COL_PERM: COL_NAME,
    KEY_PERM: KEY_NAME,
    GRP_PERM: GRP_NAME
}


class ColumnSpec(object):
    """
    A ColumnSpec defines a data column for tabular display.  In many cases it
    is simply one field from the underlying Model instance, possibly formatted
    and/or styled by CSS.  Provision is made for converting the value to a form
    suitable for sorting (this assumes the use of Python's sort() rather than
    database operations).
    
    A colspec may be permitted to be used as a dynamic column (COL_PERM), sort
    key (KEY_PERM), and/or group key (GRP_PERM).  The default is to allow COL
    and KEY.  Colspecs with NO_PERM cannot be used as dynamic columns, only as
    programatically specified "prefix" columns.

    NB: a default label will be injected by the catalog which contains the
    CoumnSpec, so although that attribute seems to default to None, in the context
    of a catalog the label will never be None.  Freestanding ColumnSpecs are
    no longer considered fully supported; if you must use such, supply label!
    """

    css_class = None

    def __init__(self, data_def, **kwargs):
        """
        ColumnSpec(data_func, **)
            data_def	  name of attribute/method or a function to extract data

        Keyword arguments:
            label         text to use as column header
            sort_func     extract sort key from row (uses data() if not defined)
            format_func   transform data for display (use as-is if not defined)
            permissions   uses allowed this spec in dynamioc columns (COL & KEY by default)
            css_class     if given, overrides class's default CSS class value
        """

        self._dd = data_def

        self._sf = kwargs.pop('sort_func', None)
        self._ff = kwargs.pop('format_func', None)
        self.label = kwargs.pop('label', None)
        self.permissions = kwargs.pop('permissions', COL_PERM+KEY_PERM)
        if 'css_class' in kwargs:
            self.css_class = kwargs['css_class']
        self.init_sum = kwargs.pop('init_sum', None)

    def data(self, row):
        """return the column's value

        The traditional form is a function that takes one argument, the "row" object,
        and returns a string or object ready for the template's auto-str.  As an
        alternative, many uses can be handled by giving the name of an attribute or
        method (taking no args other than, of course, self); this removes many trivial
        lambdas in the catalog definitions.
        """

        if callable(self._dd):
            return self._dd(row)
        dd = getattr(row, self._dd)
        return dd() if callable(dd) else dd

    def sort_key(self, row):
        "return the value to be used when sorting on this column"

        return self._sf(row) if self._sf else self.data(row)

    def format(self, data):
        "return the data formatted for display, or a renderable object"

        return self._ff(data) if self._ff else data

    def totalize(self, sum, data):
        if self.init_sum is not None:
            return sum + data


class AmountColumnSpec(ColumnSpec):
    """generic numerical amount.  These have a CSS class for alignment"""

    css_class = 'amount_col'


class DateColumnSpec(ColumnSpec):
    """date (not datetime) column.  These have a CSS class for alignment and special
    handling of NULL values to allow sorting.
    """

    css_class = 'date_col'

    def __init__(self, *args, **kwargs):
        """Accepts sort_default keyword to provide a value for sorting when the column
        has a NULL value.  Absent a sort_default arg here, NULLs will become date.max
        """

        sort_default = kwargs.pop('sort_default', None)
        ColumnSpec.__init__(self, *args, **kwargs)
        self._sort_default = sort_default

    def sort_key(self, row):
        k = ColumnSpec.sort_key(self, row)
        return k if k else self._sort_default if self._sort_default else datetime.date.max


class ColSpecOptionForm(Form):

    def __init__(self, catalog, *args, **kwargs):
        "extend Form.__init__ in order to stuff catalog-specific choices into fields"

        super(ColSpecOptionForm, self).__init__(*args, **kwargs)
        for name in CKG_NAMES:
            self.fields[name].choices = catalog.choices(name)

    # now be sure these are given the same names in CKG_NAMES!
    cols = OrderedMultipleChoiceField(required=False)
    keys = OrderedMultipleChoiceField(required=False, allow_minus=True)
    grps = OrderedMultipleChoiceField(required=False, allow_minus=True)

    view_name = CharField(required=False, widget=HiddenInput)
    qargs = CharField(required=False, widget=HiddenInput)


class ColumnSpecCatalog(object):
    "Collects a set of ColumnSpec items together in a catalog for use by parse_args"

    def __init__(self, prefix, **kwargs):
        """
        ColumnSpecCatalog('gl', key_name = aColSpec, ...)

        The key_name is taken as the catalog name of the column.  If the spec had
        no label=aString argument, the key_name with underlines changed to spaces
        ("key name", eg.) will be used.  A label="" will NOT be replaced this way!
        """

        self.prefix = prefix
        self.catalog = {}
        self.permuted = {}
        self._keys = dict((name,[]) for name in CKG_NAMES)
        for k,cs in kwargs.items():
            self._add(k, cs)

    def _add(self, key, cs):
        if cs.label is None:
            cs.label = ' '.join(key.split('_'))
        self.catalog[key] = (cs.label, cs)		### OBSOLESCENT?  not yet...
        self.permuted[cs] = key
        for perm,name in category_name.items():
            if perm in cs.permissions:
                self._keys[name].append(key)

    def __getitem__(self, key):
        "return the (label, colspec) tuple that key selects or throw KeyError"

        return self.catalog[key]

    def get_spec(self, key):
        "return just the colspec from the tuple that key selects or throw KeyError"

        return self.catalog[key][1]

    def allowed(self, key, required):
        "return a true value if identified item has permissions granting those asked for"

        if key in self.catalog and all(r in self.catalog[key][1].permissions for r in required):
            return True
        return False

    def colspec_to_key(self, colspec):
        "return the catalog key that selects the tuple with colspec or throw KeyError"

        return self.permuted.get(colspec)

    ### methods for presenting and managing table options

    def choices(self, keyset_name):
        """returns a list of (value, text) pairs in Django's "choices" list format
        
        keyset_name is one of the keys defined for self._keys (cols, keys, grps)
        """

        keyset = self._keys[keyset_name]
        return [(key,lbl) for key,(lbl,spec) in self.catalog.items() if key in keyset]

    def option_form(self, *args, **kwargs):
        return ColSpecOptionForm(self, *args, **kwargs)

    ### methods for parsing dynamic table options and laying out tables

    def parse_args(self, args, defaults):
        """return the normalized effective columns, sort keys, and group as a list each
        of catalog keys (order to match CKG_NAMES).

        args         query args' dict-like (keys are prefix + '_cols' etc.)
        defaults     default arg values in query arg dict form
        """

        def parse_one(name, permission, allow_minus):
            key = self.prefix + '_' + name
            if key in args:
                eff = args[key]
            elif key in defaults:
                eff = defaults[key]
            else:
                eff=''

            if not eff:
                return []
            if allow_minus:
                return [c for c in eff.split(',') if self.allowed(c.lstrip('-'), permission)]
            return [c for c in eff.split(',') if self.allowed(c, permission)]            

        return (
            parse_one(COL_NAME, COL_PERM, False),
            parse_one(KEY_NAME, KEY_PERM, True),
            parse_one(GRP_NAME, GRP_PERM, True)
        )

    def sort_rows(self, query, args):
        """sorts in place a list of data rows according to the specs in sortkeys
        
        NB: although the sort is done in place, the sorted list IS returned to simplify
        the expected common use where the call is the first place a queryset is
        instantiated as a list (eg., x = sort_rows(list(x_query), sortkeys)).

        sortkeys is a list of (is_rev, col_spec) pairs in order from most significant to
        least significant.  Therefore the [stable] sort is done in the reverse order!
        """

        ### FIX ME ### several thoughts
        #
        #	could combine adjacent sort specs that have the same is_rev into one sort()
        #
        #   more ambitious: could take in queryset and do at least some of the sorting
        #	in the query.  The limit here isn't opposing sort directions but the presence
        #   of non-db values that may be used in sort key columns.
        ###

        rows = list(query.all())

        for arg in reversed(args):
            key,is_rev = (arg[1:],True) if arg[0] == '-' else (arg,False)
            col_spec = self[key][1]
            rows.sort(key=lambda x: col_spec.sort_key(x), reverse=is_rev)
        return rows


    def rows_to_table(self, rows, columns, grp_args):
        """returns a list of row groups, where each group is
        
          (group_dict, (raw_item, [extra_col_item, ...]), ...)

        group_dict may be empty (no grouping), or contains at least the key "value"
        """

        def format_col(data, cs):
            fmt_data = cs.format(data)
            try:
                fmt_data['cls'] = cs.css_class
            except:
                fmt_data = (fmt_data, cs.css_class)
            return fmt_data

        group = self[grp_args[0].lstrip('-')][1] if grp_args else None

        table_groups = []
        cur_group = []
        cur_group_key = []
        totals = []
        
        def reset_group(first_row):
            if cur_group:
                x = [format_col(t, cs) if t is not None else '' for cs,t in totals]
                cur_group.append((None, x))
                table_groups.append(({'value': cur_group_key[0]} if cur_group_key else None, list(cur_group)))
                cur_group[:] = []
            if not cur_group:
                cur_group_key[:] = [group.data(first_row) if group and first_row else None]
                totals[:] = [[col, col.init_sum] for col in columns]

        if rows:
            reset_group(rows[0])
        for r in rows:
            if group:
                this_key = group.data(r)
                if this_key != cur_group_key[0]:
                    reset_group(r)
            extra = []
            for ct in totals:
                data = ct[0].data(r)
                extra.append(format_col(data, ct[0]))
                if ct[1] is not None:
                    ct[1] = ct[0].totalize(ct[1], data)
            cur_group.append((r, extra))

        reset_group(None)

        return table_groups

    def columns_to_header_list(self, columns, effective, request):
        "returns the header list for these columns - the big deal is sort-by links"

        qitems = parse_qsl(request.META.get('QUERY_STRING', ''))

        headers = []
        for title,col in columns:
            if KEY_PERM in col.permissions:
                key = self.colspec_to_key(col)
                if key and key not in effective[2]:		# exclude active groups
                    if key in effective[1]:
                        key = '-' + key
                        title = title + u'\u25B4'
                    elif ('-' + key ) in effective[1]:
                        title = title + u'\u25BE'

                    ### FIX ME ### this does not revert to an empty spec when it == default
                    #              might not be worth the trouble?  see table_options PUT

                    qargs = urlencode(update_qitems(qitems, [(self.prefix + '_' + KEY_NAME, key)]))
                    title = (title, '?' + qargs)
            headers.append((title, col.css_class))
        return headers

    def build_table(self, request, query, defaults, prefix_columns):
        """combines parse_args, sort_keys and rows_to_table in the most common fashion

        returns a dictionary with the *_headers, *_groups and *_num_cols defining the table

        query           manager or queryset that allows access to the data (may be filtered)
        request         request object from view
        default_cols    string of default 'extra' columns
        prefix_columns  optional list of (name, ColumnSpec object) columns always at start
                        NB: prefix columns NEED NOT be in the catalog, passed by object
                        (but they can't participate in the click header to sort magic)
        """

        effective = self.parse_args(request.GET, defaults)
        col_args,sort_args,grp_args = effective

        rows = self.sort_rows(query, grp_args + sort_args)

        columns = prefix_columns + [self[c] for c in col_args]

        prefix = self.prefix
        effective_options = '&'.join("%s_%s=%s" % (prefix, n, ','.join(vl)) for n,vl in
            zip(('cols', 'keys', 'grps'), effective))

        return {
            prefix + '_groups': self.rows_to_table(rows, [c[1] for c in columns], grp_args),
            prefix + '_headers': self.columns_to_header_list(columns, effective, request),
            prefix + '_num_cols': len(columns),
        }


### FIX ME ### update_qitems is probably of general usefulness... where should it live?


def update_qitems(qitems, uitems):
    """returns the item-list form of the updated query args after updating one list
    with another.  Items in qitems appear in the same order in the result (unless
    removed), and updtae items not already presetn will be appended in the order
    they appear in uitems.
    
    Q: What about repeated query arg keys (in either qitems or uitems)?
    A: What, me worry about ill-formed queries?  Undefined.  Seriously.
    """

    udict = dict((k,i) for i,(k,v) in enumerate(uitems))
    nitems = []
    for k,v in qitems:  
        if k in udict:  
            i = udict[k]
            uval = uitems[i][1]  
            uitems[i] = (k, None)
            if uval is not None:
                nitems.append((k, uval))
        else:
            nitems.append((k,v))
    nitems.extend((k,v) for k,v in uitems if v is not None)
    return nitems
