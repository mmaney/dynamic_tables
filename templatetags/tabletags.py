from cgi import escape

def esc(s): return escape(s, True)

from django import template
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe

register = template.Library()


### FILTER fmt_action


@register.filter
def fmt_actions(var, tag='li'):
    """returns the actions formatted into HTML elements

    var is a sequence of action descriptors, each of which can be
     * a tuple of values defining a menu action
     * '_' indicating a section break that becomes <hr> ifthere are adjacent items

    An action tuple contains the following parts (*=required, ?=optional, may be None)
     * the "args" flag - '0', '1', '+' or '*'
     * the "action" type - 'url, 'all' or 'none'
     ? the URL to invoke for the action, optionally with a '#' placeholder for the id(s)
     * the link text
    """

    res = []

    last_was_action = False
    for action in var:
        if action == '_':
            if last_was_action:
                res.append("<hr />")
                last_was_action = False
        else:
            act = ['<%s args="%s" action="%s"' % (tag, esc(action[0]), esc(action[1]))]
            if action[2]:
                act.append(' url="%s"' % esc(action[2]))
            act.append('><a>%s</a></%s>' % (esc(action[3]), tag))
            
            res.append(''.join(act))
            last_was_action = True

    return mark_safe('\n'.join(res))


### FILTER td_cell


@register.filter
def td_cell(var, colspan=None, autoescape=None):
    """{{ var|td_cell:colspan }}
    
    Renders the value specified by var as a table data cell: "<td ...>...</td>"
    There are various forms the raw data, var, may take - see make_cell below
    """    

    return make_cell(var, colspan, autoescape, 'td')
    
td_cell.needs_autoescape = True


### FILTER th_cell


@register.filter
def th_cell(var, colspan=None, autoescape=None):
    """{{ var|th_cell:colspan }}
    
    Same as td_cell but uses th tags.
    """

    return make_cell(var, colspan, autoescape, 'th')

th_cell.needs_autoescape = True


### FILTER cell


@register.filter
def cell(var, tag, autoescape=None):
    """{{ var|cell:tag }}

    Like th_cell and td_cell, but takes required tag name argument
    """

    return make_cell(var, None, autoescape, tag)

cell.needs_autoescape = True


### TAG modulo


@register.tag
def modulo(parser, token):
    """{% modulo val sequence|item... %}

    Renders one of the n items based on val % n.  The items may be passed as multiple
    individual arguments or as a single sequence of items.  Each item can be any valid
    "filterexpression", but most commonly they are simple strings/literals.  The usual
    use is expected to have a forloop.*counter* as the val, but that's not required.

    Why not use the builtin tag cycle?  Sometimes there is no reason.  modulo arose
    out of a desire to do consistent zebra bars in a table with subsections.  cycle
    uses its own internal cycle counter, and there's no way to reset it when starting
    a new section.  {% modulo forloop.counter0 ... %} solves that very nicely.
    """

    parts = token.split_contents()
    if len(parts) < 3:
        raise template.TemplateSyntaxError("%s tag requires value and one sequence or two or more item args" % parts[0])
    return ModuloNode(parts[1], parts[2:])


class ModuloNode(template.Node):

    def __init__(self, val, lst):
        self.val = template.Variable(val)
        if len(lst) == 1:		# it's a sequence
            self.lst = None
            self.seq = template.Variable(lst[0])
        else:
            self.lst = [template.Variable(x) for x in lst]

    def __repr__(self):
        return '<ModuloNode>'

    def render(self, context):
        if self.lst is None:
            self.lst = [template.Variable(x) for x in self.seq.resolve(context)]
        return self.lst[self.val.resolve(context) % len(self.lst)].resolve(context)


### implementation


def make_cell(var, colspan, autoescape, tag):
    """resolves the filtered value (var) and returns a safely-escaped string
    to render the value and HTML tags.  var can be a number of things:

    simple data: anything other than a tuple or dictionary is just rendered to
        a string and emitted between unadorned td /td tags
    
    (val, css_class) val is rendered as a string and emitted enclosed in tags
        <td class="css_class">...</td> (class="..." omitted if css_class is false)
    
    ((val, url), css_class) val is rendered as a string, wrapped in <a> tag if url,
        that wrapped in td tags with optional class as above  [obsolescent?]

    NEW PLAN:

    a dictionary with various keys:
        'val'        required, string or renderable object
        'cls'        optional CSS class value (string) for the wrapping tag
        'url'        optional url used to generate <a> tag around rendered value
        'url_title'  optional title attribute value for url
        'img_url'
        'img_alt'
    """

    esc = conditional_escape if autoescape else lambda x: x

    def fmt_if(fmt_str, value):
        return fmt_str % esc(value) if value else ''

    ### FIX ME ### decrease duplication in HTML construction

    if type(var) == tuple:		# 'simple' complex data - tuples (in tuples)...
        val,css_class = var
        if type(val) != tuple:
            safe_val = esc(val)
        else:
            val,url = val
            if url:
                safe_val = '<a href="%s">%s</a>' % (url, esc(val))
            else:
                safe_val = esc(val)
        safe_vals = (fmt_if(' class="%s"', css_class), safe_val)
    elif type(var) == dict:		# NEW PLAN - vlaue and adornments in dict
        safe_val = esc(var['val'])
        if 'img_url' in var:
            safe_val += '<img src="%s"%s>' % (var['img_url'],
                fmt_if(' alt="%s"', var.get('img_alt')))
        if 'url' in var:
            safe_val = '<a href="%s"%s>%s</a>' % (esc(var['url']),
                fmt_if(' title="%s"', var.get('url_title')), safe_val)
        classes = ' '.join(c for c in (var.get('cls'), var.get('class')) if c)
        safe_vals = (fmt_if(' class="%s"', classes), safe_val)

    else:
        safe_vals = ('', esc(var))

    return mark_safe("<%s%s%s>%s</%s>" % ((tag, fmt_if(' colspan="%s"', colspan)) + safe_vals + (tag,)))
